//package hotel;

import java.rmi.Naming;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;

public class HotelServer{
    public HotelServer(){
        try{
            RoomManager r = new RoomManagerImpl();

            System.out.println("Hello");

            Naming.rebind("rmi://localhost:1099/HotelService", r);


        }
        catch (Exception e){
              System.out.println("Error: _" + e);
        }

    }
    public static void main (String args []){
        // set the defaults
            new DataStorage().setRoomsData();
            new DataStorage().setBookedRooms();
            new HotelServer();
    }
}
