// package hotel;

import java.rmi.RemoteException;
import java.rmi.Remote;
import java.util.ArrayList;
import java.text.ParseException;
 

public interface RoomManager extends Remote {
    //book a room
    public boolean bookRoom(int room_type, String name) throws RemoteException;

    //print all the guests
    public ArrayList<String> listGuests() throws RemoteException;

    //print available rooms
    public ArrayList<Room> listAvailableRooms() throws RemoteException;

    //compute and print revenue
    public ArrayList<String> revenue() throws RemoteException, ParseException;
    
    }
