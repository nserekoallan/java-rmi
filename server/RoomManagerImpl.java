// package hotel;

import java.rmi.RemoteException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.text.NumberFormat;


public class RoomManagerImpl extends java.rmi.server.UnicastRemoteObject implements RoomManager {
	
	public RoomManagerImpl() throws RemoteException {
			super();
	}
	
	//book a room
     public boolean bookRoom(int room_type, String name) throws RemoteException{
		int available=0;
        // get value of key
        Room room_data = (Room)DataStorage.roomsData.get(room_type);
        Room booked_room_data=(Room)DataStorage.bookedRooms.get(room_type);

        available=room_data.getAvailable();
        if(available>0){
            available=available-1;
            DataStorage.roomsData.put(room_type,new Room(room_type,room_data.getPrice(),available));
            booked_room_data.setAvailable(booked_room_data.getAvailable()+1);
            DataStorage.bookedRooms.put(room_type,booked_room_data);
            new DataStorage().saveGuest(name);
            return true;
        }else {
            return false;
        }
	}

    //print all the guests
    public ArrayList<String> listGuests() throws RemoteException{
		//fetch All Guests
		return DataStorage.guestsList;

	}

    //print available rooms
    public ArrayList<Room> listAvailableRooms() throws RemoteException{
	// the list to store the objects
      ArrayList<Room> list=new ArrayList<>();
       // retrieving data stored
        HashMap<Integer, Room> rooms=new DataStorage().getRoomsData();
        // looping through to add each room into the list
        for(Map.Entry room : rooms.entrySet()){
           list.add((Room) room.getValue());
        }

        return list;
	
	}

    //compute and print revenue
    public ArrayList<String> revenue() throws RemoteException, ParseException{
    	// looping through to add each room into the list
        ArrayList<String> list=new ArrayList<>();
        for(Map.Entry booked_room : DataStorage.bookedRooms.entrySet()){
            Room room= (Room) booked_room.getValue();
            NumberFormat nf=NumberFormat.getInstance();
           int price= nf.parse(room.getPrice()).intValue();
           int booked_rooms=room.getAvailable();
           int revenue=price * booked_rooms;
           list.add(booked_rooms+" booked rooms of room_type "+room.getType()+" have a total revenue "+revenue+" UGX.");
        }
        return list;
		    }
}
