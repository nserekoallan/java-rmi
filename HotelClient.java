// package hotel;

import java.net.MalformedURLException;
import java.rmi.Naming;

public class HotelClient {
	public static void main (String [] args) {
		if (args.length == 0) {
			 System.out.println("USAGE:\n");
	         System.out.println("java HotelClient list <server address>: list the available number of rooms in each price");
	         System.out.println("java HotelClient book <server address> <room type> <guest name>: books a \n" +
	                    "room of the specified type (if available)\n");
	          System.out.println(" java HotelClient guests <server address>: list the names of all the registered guests");
	          System.out.println(" java HotelClient revenue <server address>: prints the revenue breakdown based on \n"+
	            "the booked rooms and their types.");
		} else {
		try {
			RoomManager r = null;
				
					//listing rooms available
					if (args[0].equals("list") && args.length==2) {
	                    String server=args[1].toString();
	                    r=(RoomManager)Naming.lookup("rmi://"+server+":1099/HotelService");
	                    System.out.println("\n");
	                   for (Room room : r.listAvailableRooms()) {
	                    int available=room.getAvailable();
	                       int room_type= room.getType();
	                       String price = room.getPrice();

	                       System.out.println(available+" rooms of type " + room_type + " are available for " + price + " UGX per night");


	                   }

	                }
	                // booking a room
	                else if (args[0].equals("book") && args.length==4){
	                    String server=args[1].toString();
	                    r = (RoomManager)Naming.lookup("rmi://"+server+":1099/HotelService");
	                    int room_type= Integer.parseInt(args[2]);
	                    String guest=args[3];
	                    

	                    if(room_type<0 || room_type>4){
	                     System.out.println("The room type isn't a valid one ;0,1,2,3,4");
	                    }
	                    else if(r.bookRoom(room_type,guest)){
	                        System.out.println("room booked by "+guest);
	                    }else{
	                        System.out.println("No available room for the selected type.");
	                    }

	                }
	                // handle guests
	                else if (args[0].equals("guests")&& args.length==2){
	                    String server=args[1].toString();
	                    r=(RoomManager)Naming.lookup("rmi://"+server+":1099/HotelService");
	                    if(r.listGuests().size()<1){
	                        System.out.println("No guest Available");
	                    }else {

	                        for (String guest : r.listGuests()) {
	                            System.out.println(guest);
	                        }
	                    }
	                }
	                //revenue 
	                else if(args[0].equals("revenue")&& args.length==2){
	                    String serverAddress=args[1].toString();
	                    r=(RoomManager)Naming.lookup("rmi://"+serverAddress+":1099/HotelService");
	                    for (String revenue : r.revenue()) {
	                        System.out.println(revenue);
	                    }
	                }
	                // for cases where the user supplied invalid arguments
	                else {
	                    System.out.println("Invalid argument : use this command  java HotelClient: to see the valid ones ");
	                }
	 
		
	} catch (Exception e) {
		System.out.println("Received Exception: ");
		System.out.println(e);
	}

}
	}
}